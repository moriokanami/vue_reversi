var stoneComponent = {
    props: ['type'],
    template: '<span v-html="content"></span>',
    computed: {
        content() {
            if(this.type == '-1') { // 黒
                return '<img src="img/black_pig.png">'
            } else if(this.type == '1') { // 白
                return '<img src="img/white_pig.png">';
            }
            return '&nbsp;'; // 空白
        }
    }
};

new Vue ({
    el: '#app',
    components: {
        stone: stoneComponent
    },
    data: {
        size: 4,
        stones: [],
        currentStoneId: -1, // -1 =>黒、1=>白
        message: '',
    },
    methods: {
        initStone() {
            var stones = [];
            var size = this.size;

            for(var x = 0; x < size; x++) {
                var stoneLine = [];
                
                for(var y = 0; y < size; y++) {
                    stoneLine.push(0); // 0は空白
                }
                stones.push(stoneLine);
            }

            this.stones = stones;
        },
        getStone(x, y)  {
            return this.stones[x-1][y-1];
        },
        copyStone() {
            return JSON.parse(JSON.stringify(this.stones));
        },
        onStoneSet(x, y) {
            if(this.stones[x-1][y-1] === 0) { // 選んだ場所が空白か
                var newStones = this.copyStone();
                newStones[x-1][y-1] = this.currentStoneId;
                this.stones = newStones;
                this.changeStone(x, y);
                this.currentStoneId *= -1; // 白黒反対にする
                console.log('onStoneSetだよ');

                var fullCount = this.size * this.size;
                var stoneCounts = this.stoneCounts();

                if(stoneCounts.total == fullCount) {

                    if(stoneCounts.black > stoneCounts.white) {

                        this.message = '<img src="img/black_pig.png">の勝ち！';

                    } else if(stoneCounts.black < stoneCounts.white) {

                        this.message = '<img src="img/white_pig.png">の勝ち！';

                    } else {

                        this.message = '引き分け！';

                    }
                }
            } else {
                alert('すでに石が置かれています');
            }
        },
        changeStone(x, y) { // 同じ色で挟まれた場合にひっくり返す
            var movingCollection = [
                {x: -1, y: -1}, // 左上
                {x: 0, y: -1}, // 上
                {x: 1, y: -1}, // 右上
                {x: 1, y: 0}, // 右
                {x: 1, y: 1}, // 右下
                {x: 0, y: 1}, // 下
                {x: -1, y: 1}, // 左下
                {x: -1, y: 0}, // 左
            ];
            var baseX = x - 1; // 置かれた石の座標：ｘ
            var baseY = y - 1; // 置かれた石の座標：ｙ
            var changingStoneId = this.currentStoneId * -1; // ひっくり返す石
            console.log('changeStoneだよ');

            for(var i = 0; i < movingCollection.length; i++) {
                var moving = movingCollection[i];
                var checkingX = baseX;
                var checkingY = baseY;
                var changingPositions = []; // ひっくり返す座標

                innerLoop:
                    for(var j = 0; j < this.size; j++) {
                        checkingX += moving.x; // チェックする場所を移動
                        checkingY += moving.y;

                        if(checkingX < 0 ||
                            checkingY < 0 ||
                            checkingX >= this.size ||
                            checkingY >= this.size) { // 盤上を出た
                                break innerLoop;
                        }

                        var checkingStoneId = this.stones[checkingX][checkingY];

                        if(checkingStoneId === this.currentStoneId) { // 自分の色だった場合
                            var newStones = this.copyStone();

                            for(var k = 0; k < changingPositions.length; k++) {
                                var changingPosition = changingPositions[k];
                                var changingX = changingPosition.x;
                                var changingY = changingPosition.y;
                                newStones[changingX][changingY] = this.currentStoneId;
                            }

                            this.stones = newStones;
                            break innerLoop;

                        } else if(checkingStoneId === changingStoneId) { // 相手の色だった場合
                            changingPositions.push({x: checkingX, y: checkingY});
                        } else { // 空白の場合
                            break innerLoop;
                        }

                    }
            }
        },
        stoneCounts() {
            var counts = {
                black: 0,
                white: 0,
                total: 0
            };
            var size = this.size;

            for(var x = 0; x < size; x++) {
                for(var y = 0; y < size; y++) {
                    var stoneId = this.stones[x][y];
                    if(stoneId === -1 || stoneId === 1) {
                        if(stoneId === -1) {
                            counts.black++;
                        } else if(stoneId === 1) {
                            counts.white++;
                        }
                        counts.total++;
                    }
                }
            }
            return counts;
        },
    },
    created() {
        this.initStone();
    }
});